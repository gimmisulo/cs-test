//
//  Comment.swift
//  CSTest
//
//  Created by Sulochana Premadasa on 2/5/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//

import UIKit
import ObjectMapper

class Comment: NSObject, Mappable {

    var postId: Int?
    var name: String?
    var email :String?
    var body: String?
    
    override init() {
        super.init()
    }
    
    required convenience init?(map : Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        postId <- map["postId"]
        name <- map["name"]
        body <- map["body"]
        email <- map["email"]
        
    }
}
