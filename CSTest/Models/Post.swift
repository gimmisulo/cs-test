//
//  Post.swift
//  CSTest
//
//  Created by Sulochana Premadasa on 2/4/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//

import UIKit
import ObjectMapper

class Post: NSObject, Mappable {
    
    var userId : Int? = 0
    var id: Int? = 0
    var title : String? = ""
    var body: String? = ""
    
    override init() {
        super.init()
    }
    
    required convenience init?(map : Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        userId <- map["userId"]
        id <- map["id"]
        title <- map["title"]
        body <- map["body"]
        
    }

}
