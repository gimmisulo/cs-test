//
//  PostDetailsViewController.swift
//  CSTest
//
//  Created by Sulochana Premadasa on 2/5/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//

import UIKit
import CoreData

class PostDetailsViewController: BaseViewController {

    var post : PostEntity!
    var user : UserEntity!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyTextView: UITextView!
    @IBOutlet weak var ownerLabel: UILabel!
    @IBOutlet weak var commentsCountLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = post.title
        bodyTextView.text = post.body
        
//        do {
//            let request =  UserEntity.fetchRequest() as NSFetchRequest <UserEntity>
//            request.predicate = NSPredicate(format: "id == %i", post.userID)
//            let fetchedResults = try context.fetch(request)
//            if let aUser = fetchedResults.first {
//               user = aUser
//               ownerLabel.text = aUser.name
//            }
//        }
//        catch {
//            print ("fetching failed", error)
//        }
//
//        do {
//            let request =  CommentEntity.fetchRequest() as NSFetchRequest <CommentEntity>
//            request.predicate = NSPredicate(format: "postId == %i", post.id)
//            let fetchedResults = try context.fetch(request)
//            if !fetchedResults.isEmpty {
//                commentsCountLabel.text = "Number of comments : \(fetchedResults.count). "
//            }
//        }
//        catch {
//            print ("fetching failed", error)
//        }
        
   //This is another way (Using relationship to set data)
        
        ownerLabel.text = post.owner?.name
        commentsCountLabel.text = "Number of comments : \(String(describing: post.review!.count) )."
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showUserSegue" {
            let udvc = segue.destination as! UserDetailViewController
//            udvc.user = user
            udvc.user = post.owner ////This is another way (Using relationship to set data)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
