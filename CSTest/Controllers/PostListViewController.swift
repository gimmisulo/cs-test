//
//  PostListViewController.swift
//  CSTest
//
//  Created by Sulochana Premadasa on 2/4/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//

import UIKit
import Alamofire
import CoreData
import AlamofireObjectMapper

class PostListViewController: BaseViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    private var posts = [PostEntity]()
    private var users = [UserEntity]()
    private var comments = [CommentEntity]()
    
    private var fetchedPost: NSFetchedResultsController<PostEntity>!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = 55.0
        
        if Connectivity.isConnectedToInternet {
            
            self.deleteExistingComments()
            self.getAndSaveUsers()
            
            
        }else{
            print("No! internet is un available.")
            self.fetchAndListPosts()
        }
        
    }
    
    //**TABLE VIEW**
    
    // table view datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostCell") as! PostTableViewCell
        cell.postTitle.text = posts[indexPath.row].title
        return cell
        
    }
    
    // table view delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let request = PostEntity.fetchRequest() as NSFetchRequest<PostEntity>
        request.sortDescriptors = []
        do {
            fetchedPost = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            try fetchedPost.performFetch()
            performSegue(withIdentifier: "showPostSegue", sender: indexPath)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
  
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPostSegue" {
            if let index = sender as? IndexPath {
                let pvc = segue.destination as! PostDetailsViewController
                let post = fetchedPost.object(at: index)
                pvc.post = post
            }
        }
    }
    
    
    // **POSTS**
    func getAndSavePosts()  {
        
        Alamofire.request(postListURL).responseArray { (response: DataResponse<[Post]>) in
            
            switch response.result{
                
            case .success(let postsList):
                
                for eachPost : Post in postsList{
                    
                    let post = PostEntity(entity: PostEntity.entity(), insertInto: self.context)
                    
                    post.userID = Int16(Int(eachPost.userId!))
                    post.id = Int16(Int(eachPost.id!))
                    post.title = eachPost.title
                    post.body = eachPost.body
                    
                    ///// This is another way (Using relationship to set data)
                    do {
                        let request =  UserEntity.fetchRequest() as NSFetchRequest <UserEntity>
                        request.predicate = NSPredicate(format: "id == %i", eachPost.userId!)
                        let fetchedResults = try self.context.fetch(request)
                        if let aUser = fetchedResults.first {
                            post.owner = aUser
                        }
                    }
                    catch {
                        print ("fetching failed", error)
                    }
                    /////

                    do {
                        try self.context.save()
                    } catch {
                        print("Failed saving")
                    }
                }
                
                self.getAndSaveComments()
                
            case .failure(let error):
                print(error)
                break;
            }
            
        }
        
    }
    
    // fetch and list posts
    func fetchAndListPosts() {
        do {
            posts = try context.fetch(PostEntity.fetchRequest())
            self.tableView.reloadData()
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    // Delete existing posts
    func deleteExistingPosts() {
        
        do {
            let results = try context.fetch(PostEntity.fetchRequest())
            for managedObject in results
            {
                self.context.delete(managedObject as! NSManagedObject)
            }
            self.deleteExistingUsers()
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
        }
        
    }
    
    
    // **USERS**
    func getAndSaveUsers()  {
        
        Alamofire.request(usersURL).responseArray { (response: DataResponse<[User]>) in
            
            switch response.result{
                
            case .success(let usersList):
                
                for eachUser : User in usersList{
                    
                    let user = UserEntity(entity: UserEntity.entity(), insertInto: self.context)
                    
                    user.id = Int16(Int(eachUser.id!))
                    user.name = eachUser.name
                    user.username = eachUser.userName
                    user.email = eachUser.email
                    user.street = eachUser.address?.street
                    user.suite = eachUser.address?.suite
                    user.city = eachUser.address?.city
                    user.zipcode = eachUser.address?.zipcode
                    user.lat = eachUser.address?.geo?.latitude
                    user.lng = eachUser.address?.geo?.longitude
                    user.phone = eachUser.phone
                    user.website = eachUser.website
                    user.companyName = eachUser.company?.name
                    user.catchPhrase = eachUser.company?.catchPhrase
                    user.bs = eachUser.company?.bs
                    
                    do {
                        try self.context.save()
                    } catch {
                        print("Failed saving")
                    }
                }
                self.getAndSavePosts()

            case .failure(let error):
                print(error)
                break;
            }
            
        }
        
    }
    
    // fetch users
    func fetchUsers() {
        do {
            users = try context.fetch(UserEntity.fetchRequest())
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    // Delete existing users
    func deleteExistingUsers() {
        
        do {
            let results = try context.fetch(UserEntity.fetchRequest())
            for managedObject in results
            {
                self.context.delete(managedObject as! NSManagedObject)
            }
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
        }
        
    }
    
    // **COMMENTS**
    
    func getAndSaveComments()  {
        
        Alamofire.request(commentsURL).responseArray { (response: DataResponse<[Comment]>) in
            
            switch response.result{
                
            case .success(let commentList):
                
                for eachComment : Comment in commentList{
                    
                    let comment = CommentEntity(entity: CommentEntity.entity(), insertInto: self.context)

                    comment.postId = Int16(Int(eachComment.postId!))
                    comment.name = eachComment.name
                    comment.email = eachComment.email
                    comment.email = eachComment.email
                    
                    //////// This is another way (Using relationship to set data)
 
                    do {
                        let request =  PostEntity.fetchRequest() as NSFetchRequest <PostEntity>
                        request.predicate = NSPredicate(format: "id == %i", eachComment.postId!)
                        let fetchedResults = try self.context.fetch(request)
                        if !fetchedResults.isEmpty {
                           comment.post = fetchedResults.first
                        }
                    }
                    catch {
                        print ("fetching failed", error)
                    }
 
                    //////////
                    
                    do {
                        try self.context.save()
                    } catch {
                        print("Failed saving")
                    }
                }
              self.fetchAndListPosts()
            case .failure(let error):
                print(error)
                break;
            }
            
        }
        
    }
    
    // fetch Comments
    func fetchComments() {
        do {
            comments = try context.fetch(CommentEntity.fetchRequest())
        
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    // Delete existing comments
    func deleteExistingComments() {
        
        do {
            let results = try context.fetch(CommentEntity.fetchRequest())
            for managedObject in results
            {
                self.context.delete(managedObject as! NSManagedObject)
            }
            self.deleteExistingPosts()
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
        }
        
    }
    
    
}

class PostTableViewCell: UITableViewCell {
    @IBOutlet weak var postTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
