//
//  UserEntity+CoreDataProperties.swift
//  CSTest
//
//  Created by Sulochana Premadasa on 2/5/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//
//

import Foundation
import CoreData


extension UserEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserEntity> {
        return NSFetchRequest<UserEntity>(entityName: "UserEntity")
    }

    @NSManaged public var bs: String?
    @NSManaged public var catchPhrase: String?
    @NSManaged public var city: String?
    @NSManaged public var companyName: String?
    @NSManaged public var email: String?
    @NSManaged public var id: Int16
    @NSManaged public var lat: String?
    @NSManaged public var lng: String?
    @NSManaged public var name: String?
    @NSManaged public var phone: String?
    @NSManaged public var street: String?
    @NSManaged public var suite: String?
    @NSManaged public var username: String?
    @NSManaged public var website: String?
    @NSManaged public var zipcode: String?
    @NSManaged public var posts: NSSet?

}

// MARK: Generated accessors for posts
extension UserEntity {

    @objc(addPostsObject:)
    @NSManaged public func addToPosts(_ value: PostEntity)

    @objc(removePostsObject:)
    @NSManaged public func removeFromPosts(_ value: PostEntity)

    @objc(addPosts:)
    @NSManaged public func addToPosts(_ values: NSSet)

    @objc(removePosts:)
    @NSManaged public func removeFromPosts(_ values: NSSet)

}
