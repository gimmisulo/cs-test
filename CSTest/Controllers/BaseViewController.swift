//
//  BaseViewController.swift
//  CSTest
//
//  Created by Sulochana Premadasa on 2/4/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import CoreData

class BaseViewController: UIViewController {
    
    var appDelegate = UIApplication.shared.delegate as! AppDelegate
    public let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let postListURL = "http://jsonplaceholder.typicode.com/posts"
    let usersURL = "http://jsonplaceholder.typicode.com/users"
    let commentsURL = "http://jsonplaceholder.typicode.com/comments"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
