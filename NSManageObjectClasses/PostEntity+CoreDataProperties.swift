//
//  PostEntity+CoreDataProperties.swift
//  CSTest
//
//  Created by Sulochana Premadasa on 2/5/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//
//

import Foundation
import CoreData


extension PostEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PostEntity> {
        return NSFetchRequest<PostEntity>(entityName: "PostEntity")
    }

    @NSManaged public var body: String?
    @NSManaged public var id: Int16
    @NSManaged public var title: String?
    @NSManaged public var userID: Int16
    @NSManaged public var owner: UserEntity?
    @NSManaged public var review: NSSet?

}

// MARK: Generated accessors for review
extension PostEntity {

    @objc(addReviewObject:)
    @NSManaged public func addToReview(_ value: CommentEntity)

    @objc(removeReviewObject:)
    @NSManaged public func removeFromReview(_ value: CommentEntity)

    @objc(addReview:)
    @NSManaged public func addToReview(_ values: NSSet)

    @objc(removeReview:)
    @NSManaged public func removeFromReview(_ values: NSSet)

}
