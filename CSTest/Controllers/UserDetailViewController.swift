//
//  UserDetailViewController.swift
//  CSTest
//
//  Created by Sulochana Premadasa on 2/5/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//

import UIKit
import MapKit

class UserDetailViewController: UIViewController {
    
    var user : UserEntity!
    
    @IBOutlet weak var userInfoTextView: UITextView!
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadMap()
        self.setInfo()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setInfo(){
        let defaultVal = "Not Given"
        userInfoTextView.text = "NAME : \(user.name ?? defaultVal)\n\nEMAIL : \(user.email ?? defaultVal)\n\nADDRESS:\nStreet : \(user.street ?? defaultVal)\nSuite : \(user.suite ?? defaultVal)\nCity : \(user.city ?? defaultVal)\n\nZIP CODE : \(user.zipcode ?? defaultVal)\n\nPHONE : \(user.phone ?? defaultVal)\n\nWEB SITE : \(user.website ?? defaultVal)\n\nCOMPANY:\nName : \(user.companyName ?? defaultVal)\nCatch Phrase : \(user.catchPhrase ?? defaultVal)\nBs : \(user.bs ?? defaultVal)"
    }

    func loadMap(){
        let CLLCoordType = CLLocationCoordinate2D(latitude: (user.lat! as NSString).doubleValue,
                                                  longitude: (user.lng! as NSString).doubleValue)
        let anno = MKPointAnnotation();
        anno.coordinate = CLLCoordType;
        mapView.addAnnotation(anno);
//        mapView.setCenter(CLLCoordType, animated: true)
        
        let camera = MKMapCamera(lookingAtCenter: CLLCoordType, fromDistance: 5.0, pitch: 5.0, heading: 0.0)
        mapView.setCamera(camera, animated: true)
    }
    

}
