//
//  User.swift
//  CSTest
//
//  Created by Sulochana Premadasa on 2/4/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//

import UIKit
import ObjectMapper

class User: NSObject,Mappable {
    
    var id: Int?
    var name : String?
    var userName: String?
    var email :String?
    var address : Address?
    var phone: String?
    var website :String?
    var company : Company?
    
    override init() {
        super.init()
    }
    
    required convenience init?(map : Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        userName <- map["username"]
        email <- map["email"]
        address <- map["address"]
        phone <- map["phone"]
        website <- map["website"]
        company <- map["company"]
    }
}

class Address: NSObject , Mappable{
    
    var street : String?
    var suite: String?
    var city :String?
    var zipcode :String?
    var geo : Coordinates?
    
    override init() {
        super.init()
    }
    
    required convenience init?(map : Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        street <- map["street"]
        suite <- map["suite"]
        city <- map["city"]
        zipcode <- map["zipcode"]
        geo <- map["geo"]
    }
    
}


class Coordinates: NSObject , Mappable{
    
    var longitude : String?
    var latitude : String?
    
    override init() {
        super.init()
    }
    
    required convenience init?(map : Map) {
        self.init()
    }
    func mapping(map: Map) {
        longitude <- map["lng"]
        latitude <- map["lat"]
        
    }
    
}

class Company: NSObject , Mappable{
    
    var name : String?
    var catchPhrase: String?
    var bs :String?

    
    override init() {
        super.init()
    }
    
    required convenience init?(map : Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        name <- map["name"]
        catchPhrase <- map["catchPhrase"]
        bs <- map["bs"]

    }
    
}
