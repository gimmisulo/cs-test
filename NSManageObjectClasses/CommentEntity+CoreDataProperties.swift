//
//  CommentEntity+CoreDataProperties.swift
//  CSTest
//
//  Created by Sulochana Premadasa on 2/5/18.
//  Copyright © 2018 Sulochana Premadasa. All rights reserved.
//
//

import Foundation
import CoreData


extension CommentEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CommentEntity> {
        return NSFetchRequest<CommentEntity>(entityName: "CommentEntity")
    }

    @NSManaged public var postId: Int16
    @NSManaged public var name: String?
    @NSManaged public var email: String?
    @NSManaged public var body: String?
    @NSManaged public var post: PostEntity?

}
